

import os 
import argparse
#La bibliothèque permettant de lire les fichiers rosbag (ROS1 Noetic)
from bagpy import bagreader
import pandas as pd
import shutil

def save_rosbags_topics(file_name : str, directory :  str):
	
	"""
	This function export all the informations from the ROSBAG into a csv file
	which contains the topics, the variable types, the number of samples and the frequency
	samples. The most interesting part it is the topic name inorder to extract all the data 
	saved in this topic
	"""
	if(directory not in os.listdir()): 
		os.mkdir(directory)
	else: 
		shutil.rmtree(directory)
		os.mkdir(directory)

	for content in file_name: 

		content_split = content.split("/")[-1]
		
		
		b = bagreader(content)
		df_topics = b.topic_table
		df_topics.to_csv(os.path.join(directory,content_split[:-5]+'.csv'))
		print("Save the topics in csv format : {} and the name is {}".format(directory,content_split[:-5]+'.csv'))

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser(description="Export all topics into a csv format")
	parser.add_argument('filenames' , type=str , nargs='+' , help='Can support read one or multplies bagfile')
	parser.add_argument('-d' , '--output_directory' , help='Folder Name to export all csv topics files')


	args = parser.parse_args()
	save_rosbags_topics(args.filenames , args.output_directory)
	
	

from scipy.io import savemat
from bagpy import bagreader
import bagpy
import pandas as pd
import os 
import argparse
import shutil


def convert_topics_to_file(files , topics , directory):
  

  """
  This function allows to extract the data published in the topics choosen in the command line and save all the topics choosen in a new directory
  which in this directory there are each new directory containing the data topics save into a csv and matlab format.
  """

  if(directory not in os.listdir()):
    os.mkdir(directory)
  else:
    shutil.rmtree(directory)
    os.mkdir(directory)

  for file  in files:
    b = bagreader(file)
   
    for topic in topics: 
      try:

        data_frame_string = b.message_by_topic(topic)
        data_frame= pd.read_csv(data_frame_string)
        file_split = file.split('/')[-1][:-4]
        if(file_split not in os.listdir(directory)):
          os.mkdir(os.path.join(directory , file_split))
        else:
          shutil.rmtree(os.path.join(directory , file_split))
          os.mkdir(os.path.join(directory , file_split))

        data_frame.to_csv(os.path.join(directory , file_split , file_split+'.csv'))
        print("Convert the topic {} to a csv format in the directory {}.csv".format(topic , os.path.join(directory , file_split)))
        data_mat = data_frame.to_dict('list')
        topic_name = topic.replace('/' , '_')
        savemat(os.path.join(directory , file_split , file_split+'.mat') , data_mat)
        print("Convert the topic {} to a matlab format in the directory {}".format(topic , os.path.join(directory , file_split+'.mat')))


      except Exception as e: 

        print('Unable to read the topic or the file speciefied ...Error :  {}'.format(e))



if __name__ == "__main__": 

  parser = argparse.ArgumentParser(description="Export all topics into a csv format or matlab format")

  parser.add_argument('files' , nargs='+' , type=str , help="Read the rosbag files")
  parser.add_argument('-t', '--topics' , nargs='+' , help="Read different topics save to the rosbags")
  parser.add_argument('-d' , '--directory_output' , help="Save the output data" )

  args = parser.parse_args()
  print(args.files , args.topics , args.directory_output)

  convert_topics_to_file(args.files , args.topics , args.directory_output)

      







	
